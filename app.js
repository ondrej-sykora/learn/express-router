const express = require('express');
const app = express();

app.get('/', (req, res) => res.send('Handling a GET request.'));

app.post('/', (req, res) => res.send('Handling a POST request.'));

app.put('/', (req, res) => res.send('Handling a PUT request.'));

app.delete('/', (req, res) => res.send('Handling a DELETE request.'));

app.patch('/', (req, res) => res.send('Handling a PATCH request.'));

app.listen(3000, () => console.log('Application listening on port 3000!'));