# Express

Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.

More information you can find at the [expressjs.com](http://expressjs.com/) official website.

## Basic Routing

***Routing*** refers to determining how an application responds to a client request to a particular endpoint, which is a URI (or path) and a specific HTTP request method (GET, POST, and so on).

Each route can have one or more handler functions, which are executed when the route is matched.

Route definition takes the following structure:

```javascript
app.METHOD(PATH, HANDLER)
```

Where:
* app is an instance of express.
* METHOD is an HTTP request method, in lowercase.
* PATH is a path on the server.
* HANDLER is the function executed when the route is matched.

More about that you can find at [expressjs.com/en/starter/basic-routing.html](http://expressjs.com/en/starter/basic-routing.html)

# Express Router

In this tutorial we will try to create an application that will be able to handle different routes and provide different content based on that routes.

As we mentioned last time, we will use the [Express - Server](https://gitlab.com/ondrej-sykora/learn/express-server) project as the starting point. So take a look on it if you didn't come from out of there.

Based on our starting point, we have:
* express package installed
* app created, that can listen on port 3000
* we have already one route created (our 'Hello world!')

```javascript
const express = require('express');
const app = express();

app.get('/', (req, res) => res.send('Hello world!'));

app.listen(3000, () => console.log('Application listening on port 3000!'));
```

## HTTP request methods

HTTP defines a set of request methods to indicate the desired action to be performed for a given resource. Although they can also be nouns, these request methods are sometimes referred as HTTP verbs. Each of them implements a different semantic, but some common features are shared by a group of them: e.g. a request method can be safe, idempotent, or cacheable.

Like is described at [wikipedia.org/wiki/Hypertext_Transfer_Protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods)

For us is important that there are some HTTP request types and each of them has different implementation. And it's up to us to do it right.

* GET - Requests using `GET` should only retrieve data.
* POST - The `POST` method is used to submit an entity.
* PUT - The `PUT` method replaces current with the request payload.
* DELETE - I think it is quite clear what `DELETE` method does.
* PATCH - The `PATCH` method is used to apply partial modifications (That is sometimes a little bit tricky).

*(most used)*

By the way check the first letters of each of those operations: POST - **C**reate, GET - **R**ead, PUT - **U**pdate, DELETE - **D**elete. It sounds familiar, isn't it?

## How to handle those different methods

Well, I understand now, but how should I code it to get it works, are you asking? Actually, it is simpler than it looks. Above, we mentioned that we already have one routing created. Yes it is that little `get()` function. It's nothing but routing settings for `GET` requests.

### GET

We will update `app.js` file to make it bit clearer.

```javascript
app.get('/', (req, res) => res.send('Handling a GET request.'));
```

You can check the result by run `app.js` with Node.js.

```cmd
> node app.js
```

### POST

Although we still can send a `POST` request through a browser (using the AJAX call), I recommend using the external tool. In my case I'm using the Postman 👨‍🚀 that can create all types of HTTP requests and it have an ability to save your request to the collection.

More information about Postman could be found at [www.getpostman.com](https://www.getpostman.com/) 🚀

In order to process the `POST` request, we must first implement the functionality.

```javascript
app.post('/', (req, res) => res.send('Handling a POST request.'));
```

Before we can see the result we must restart the application by typing `CTRL + C` and `node app.js` in terminal. Send your `POST` request and now... Yep, we can see the 'Handling a POST request.' text response.

## PUT, DELETE, PATCH,...

We can code the others request methods by the same way.

```javascript
app.put('/', (req, res) => res.send('Handling a PUT request.'));

app.delete('/', (req, res) => res.send('Handling a DELETE request.'));

app.patch('/', (req, res) => res.send('Handling a PATCH request.'));
```

Now after restart the application, all implemented request methods should work 🤞.

We can implement this in the real world application API for various entity types at various routes eg. 

* http://my-api.com/person
* http://my-api.com/company
...

# Conclusion

Yeah, that was realy simple and fast 😛. But it is important to understand how simply we can handle these basic HTTP request, because if you will ever be creating API you will have to use these methods.

Indeed, when you will be creating complex API/Web application you can do that more clever than paste all that routing code to the one file and we will show one of these available approach (mostlikely with TypeScript ❤️) in the one of the next tutorials.

👋 See you next time.